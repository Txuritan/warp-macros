# Warp Macros

Attribute based routing macros for [Warp](https://github.com/seanmonstar/warp).

## Examples

```rust
// GET /hello/warp => 200 OK with body "Hello, warp!"
#[warp_macros::get("/hello/{name}")]
fn hello(name: String) -> impl warp::Reply {
    format!("Hello, {}!", name)
}

#[tokio::main]
async fn main() {
    warp::serve(hello)
        .run(([127, 0, 0, 1], 3030))
        .await;
}
```

### Expanded

Before macro expansion:

```rust
#[warp_macros::get("/hello/{name}")]
fn hello(name: String) -> impl warp::Reply {
    format!("Hello, {}!", name)
}
```

After macro expansion:

```rust
fn hello() -> impl warp::Filter<Extract = impl warp::Reply, Error = warp::Rejection> + std::clone::Clone {
    #[inline(always)]
    fn inner(name: String) -> impl warp::Reply {
        format!("Hello, {}!", name)
    }

    #[allow(unused_imports)]
    {
        use warp::Filter;

        warp::filters::method::get()
            .and(warp::filters::path::path("hello"))
            .and(warp::filters::path::param::<String>())
            .and(warp::filters::path::end())
            .map(|name| inner(name))
    }
}
```
